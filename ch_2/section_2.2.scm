;; geiser
(define (append list1 list2)
  (if (null? list1)  ;; if list is empty then the result is the second list
      list2
      (cons (car list1)  ;; otherwise cons the tip of list 1 to the append of the sub lists. (recursive)
            (append (cdr list1) list2))))

;; ex 2.17

(define (last-pair listt)
  (if (null? (cdr listt))
	     (car listt)
	     (last-pair (cdr listt))))

(last-pair (list 2 3 4 5))

;; ex 2.18
(define (reverse listt)
  (if (null? listt)  ;; if list is empty then.. no reverse. 
             listt  ;; append the car of the list to the reversal of the cdr of the list
             (append (reverse (cdr listt)) (list (car listt)))))

(reverse (list 1 2 3 4))

;; ex 2.19
(define no-more? null?)

(define first-denomination car)

(define except-first-denomination cdr)

(define (cc amount coin-values)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (no-more? coin-values)) 0)
        (else
         (+ (cc amount
                (except-first-denomination coin-values))
            (cc (- amount (first-denomination coin-values))
                coin-values)))))


(define us-coins (list 50 25 10 5 1))

(define uk-coins (list 100 50 20 10 5 2 1 0.5))

(cc 100 us-coins)

(cc 100 uk-coins)

;; ex 2.20
(define (same-parity-helper parity-checker nums)
  (cond ((null? nums)  '())
        ((parity-checker (car nums)) (cons (car nums)
                                  (same-parity-helper parity-checker (cdr nums))))
        (else (same-parity-helper parity-checker (cdr nums)) )))

(same-parity-helper even? (list 1 2 3 4 5 5 5 6 ))


(define (same-parity head_num . tail_nums)
  (cond ((even? head_num) (same-parity-helper even? (cons head_num tail_nums)))
        ((odd? head_num) (same-parity-helper odd? (cons head_num tail_nums)))))

(same-parity 2 3 4 5 6 7 7 9)

;; ex 2.21

;; a very limited "map" implementation that takes a procedure and a
;; list and applied that procedure to all the elements of that list

(define nil '()) ;; nil is not defined in scheme. its basically an empty list


(define (mapp proc listt)
  (if (null? listt)
      nil
      (cons (proc (car listt))
	    (mapp proc (cdr listt)))))

(mapp abs (list -1 2 45))

(mapp (lambda (x) (* x x))
      (list 1 2 3 4 5))

(map (lambda (x y) (* x y))
     (list 1 2 3 4)
     (list 2 3 4 5))


;; (square-list (list 1 2 3 4))
;; (1 4 9 6)

(define (square x) (* x x))

(define (square-list1 items)
  (if (null? items)
      nil
      (cons (square (car items))
	    (square-list1 (cdr items)))))

(square-list1 (list 1 2 3 4))

(define (square-list2 items)
  (map square items))

(square-list2 (list 1 2 3 4))



;; ex 2.22
;; find error in these functions and explain their behaviour

(define (square-list3 items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons (square (car things))
		    answer))))
  (iter items nil))


(square-list3 (list 1 2 3 4))
;; (16 9 4 1)

;; this happens because in the call 
;; 
;;	(iter (cdr things)
;;	      (cons (square (car things))
;;		    answer))))
;;
;; over here the the "cons" part is creates a pair with the first
;; element of the list and nil in the first iteration and in
;; subsequent iterations it creates pair with the previous pair (1,
;; nil) and the square of the next item.



(define (square-list4 items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons answer
		     (square (car things))))))
  (iter items nil))

(square-list4 (list 1 2 3 4))

;; This doesnt work either because the problem is not the order in
;; which the pairs are created but in the fact that the pairs are
;; created, but in the fact that we are creating pairs with the square
;; of the car of the list and the answer of the previous square
;; operation performed on the car of the list


;; ex 2.23

(for-each (lambda (x) (newline) (display x))
          (list 57 321 88))


;; write a new func that produces the same output as this

(define (for-each1 func items)
  (cond ((null? items) #t)
	(else (func (car items))
	      (for-each1 func (cdr items)))))


(for-each1 (lambda (x) (newline) (display x))
	   (list 2 3 4))


;; ex 2.26

(define x (list 1 2 3))

(define y (list 4 5 6))

(append x y)

(cons x y)

(list x y)


;; ex 2.27
(define (deep-reverse a_pair)
  (if (not (pair? a_pair))
      a_pair
      (list (deep-reverse (car (cdr a_pair)))
            (deep-reverse (car a_pair)))))

(define x (list (list 1 2) (list 3 4)))

(deep-reverse x)


;; ex 2.28
(define (fringe tree)
  (if (pair? tree)
      (append (fringe (car tree))
              (fringe (car (cdr tree))))
      (list tree)))

(fringe (list x x))

;; ex 2.29

(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))


;; a
(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (car (cdr mobile)))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (car (cdr branch)))

;; b
(define (weight? structure)
  (not (pair? structure))) ;; if structure is a pair then its a mobile. otherwise its a weight

(define (total-weight structure)
  (cond ((null? structure) 0)
        ((weight? structure) structure)
        (else
         (+ (total-weight (branch-structure (left-branch structure)))
            (total-weight (branch-structure (right-branch structure)))))))

(define a (make-mobile (make-branch 2 3) (make-branch 2 3))) 

(total-weight a)

;; c
(define (torque branch)
  (* (branch-length branch) (total-weight (branch-structure branch))))

(define (balanced? mobile)
  (if (not (pair? mobile))
      (= 2 2) ;; balanced by default and this is true
      (and
       (= (torque (left-branch mobile))
          (torque (right-branch mobile)))
       (balanced? (branch-structure (left-branch mobile)))
       (balanced? (branch-structure (right-branch mobile))))))

(define d (make-mobile (make-branch 10 a) (make-branch 12 5)))

(balanced? d)


;; 2.30

(define (square-tree tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (square-tree sub-tree)
             (* sub-tree sub-tree)))
       tree))

(square-tree
 (list 1
       (list 2 (list 3 4) 5)
       (list 6 7)))

;; 2.31

(define (tree-map proc tree)
  (cond ((null? tree) nil)
        ((not (pair? tree)) (proc tree))
        (else (cons (tree-map proc (car tree))
                    (tree-map proc (cdr tree))))))

(define (square x)
  (* x x))

(define (square-tree2 tree)
  (tree-map square tree))

(square-tree2
 (list 1
       (list 2 (list 3 4) 5)
       (list 6 7)))


;; 2.32

(define (subsets s)
  (if (null? s)
      (list (list))
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (x) (cons (car s) x))
                      rest)))))



(subsets (list 1 2 3))

;; 2.33
;; helper functions

;; How accumulate works in interesting. It works by accepting a
;; operation, an intial result and a seq.

;; The operation is an interesting one here
;; One argument of the operation is an element of the list.
;; The other argument is the result of the accumulate on the rest of the list

(define nil '())

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (filter predicate sequence)
  (cond ((null? sequence) nil)
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

;; actual 2.33

(define (my-map p sequence)
  (accumulate (lambda (x y) (cons (p x) y))
              nil
              sequence))

(my-map (lambda (x) (* x x)) '(1 2 3))

(define (append seq1 seq2)
  (accumulate cons
              seq2
              seq1))

(append '(1 2 3) '(4 5 6))

(define (length sequence)
  (accumulate (lambda (x y) (+ 1 y)) 0 sequence))

(length (list 1 2 3))


;; 2.34

(define (homer-eval x coeffitient-sequence)
  (accumulate (lambda (this-coeff higher-terms) (+ this-coeff (* x higher-terms)))
              0
              coeffitient-sequence))

(homer-eval 2 (list 1 3 0 5 0 1))
;; ans : 79

(define (enumerate-tree tree)
  (cond ((null? tree) nil)
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))
;; ex 2.35
(define (count-leaves t)
  (accumulate (lambda (x y) (+ 1 y))
              0
              (map (lambda (x) x)
                   (enumerate-tree t))))

(define x (cons (list 1 2) (list 3 4)))


(count-leaves '(x x))
;; ans 8

;; ex 2.36

;; It is important to note the role of map here. Map applies the
;; function car to all the elements in the sequence (which in this
;; case happens to be a sequence itself) and then produces a list with
;; the results.

(define s (list (list 1 2 3 4) (list 5 6 7 8) (list 9 10 11 12) (list 1 2 2 2)))

(define (firsts seqs)
  (map car seqs))

(define (rest seqs)
  (map cdr seqs))

(firsts s)

(rest s)

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      nil
      (cons (accumulate op init (firsts seqs))
            (accumulate-n op init (rest seqs)))))


(accumulate-n + 0 s)


;; 2.37

;; We will implement matric operations

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (x) (dot-product x v)) m))

(matrix-*-vector s (list 1 2 3 4))

(define (transpose mat)
  (accumulate-n cons nil mat))

(transpose s)

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map  (lambda (row) (matrix-*-vector cols row)) m)))


(matrix-*-matrix s s)

;; 2.38

(define (fold-left op initial sequence)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (op result (car rest))
              (cdr rest))))
  (iter initial sequence))

(define fold-right accumulate)

(fold-left / 1 (list 1 2 3))
;; ans : 1/6 

(fold-right / 1 (list 1 2 3))
;; ans = 3/2

(fold-left list 1 (list 1 2 3))
;; (((1 1) 2) 3)

(fold-right list 1 (list 1 2 3))
;; (1 (2 (3 1)))

;; The property of op that we that is commutative.  If the order of
;; the application of the operands do not matter then fold-left and
;; fold-right will give the same results

;; 2.39

;; reverse a list
(define (reverse l)
  (if (null? l)
      l
      (append (reverse (cdr l))
            (list (car l)))))

(reverse (list 1 2 3))

(define (reverse-f-r sequence)
  (fold-right (lambda (x y) (append y (list x))) nil sequence))

(define (reverse-f-l sequence)
  (fold-left (lambda (x y) (append (list y) x)) nil sequence))

(reverse-f-r (list 1 2 3))

(reverse-f-l (list 1 2 3))

;; The difference is the functions is due to how fold left and fold
;; right work.  In fold left we apply the operator to the result of the
;; previous operations and the first element on the list

;; In fold right we apply the operator to the first element of the
;; list and the result

;; permutations

(define (enumerate-interval low high)
  (if (> low high)
      nil
      (cons low (enumerate-interval (+ low 1) high))))

(enumerate-interval 1 4)

(define nil (list))

(define (flatmap proc seq)
  (accumulate append nil (map proc seq)))

(define (remove item seq)
  (filter (lambda (x) (not (= x item)))
          seq))

(define (permutations s)
  (if (null? s)
      (list nil)
      (flatmap (lambda (x)
                 (map (lambda (p) (cons x p))
                      (permutations (remove x s))))
               s)))


(flatmap (lambda (i)
           (map (lambda (j) (list i j))
                (enumerate-interval 1 (- i 1))))
         (enumerate-interval 1 5))

(permutations (list 1 2 3 4))

(remove 2 (list 1 2 3))


;; Helper functions for 2.40

(define (zero-rem? num1 num2)
  (= (remainder num1 num2) 0))

(define (prime? num)
  (null? (filter (lambda (x) (zero-rem? num x))
                 (enumerate-interval 2 (sqrt num)))))

(define (prime-sum? pair)
  (prime? (+ (car pair) (cadr pair))))

(define (make-pair-sum pair)
  (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))

(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum?
               (flatmap
                (lambda (i)
                  (map (lambda (j) (list i j))
                       (enumerate-interval 1 (- i 1))))
                (enumerate-interval 1 n)))))

(prime-sum-pairs 5)

(define (make-pair-map num)
  (map (lambda (i)
         (map (lambda (j) (list i j))
              (enumerate-interval 1 (- i 1))))
       (enumerate-interval 1 num)))

(make-pair-map 6)

;; this is the non faltmap version of make-pair.
;; it is interesting to note that in this version 

(define (make-pair num)
  (flatmap (lambda (i)
         (map (lambda (j) (list i j))
              (enumerate-interval 1 (- i 1))))
       (enumerate-interval 1 num)))

(make-pair 6)

;; ex 2.40

(define (unique-pairs num)
  (flatmap (lambda (i)
                   (map (lambda (j) (list i j))
                        (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 num)))

(unique-pairs 5)

(define (prime-sum-pairs2 num)
  (map make-pair-sum
        (filter prime-sum?
                (unique-pairs num))))

(prime-sum-pairs2 5)

;; ex 2.41

(define (unique-triples n) 
     (flatmap (lambda (k) 
                 (map (lambda (pair) 
                         (cons k pair)) 
                      (unique-pairs (- k 1)))) 
              (enumerate-interval 1 n))) 

(unique-triples 4)

(define (orderred-triple-sum num sum)
  (filter (lambda (x) (= sum (accumulate + 0 x))) (unique-triples num)))


(orderred-triple-sum 5 10)

(unique-pairs 4)

;; there is another way to generate the triples.
(flatmap (lambda (i)
           (map (lambda (j)
                  (map (lambda (k) (list i j k))
                       (enumerate-interval 1 (- j 1))))
                (enumerate-interval 1 (- i 1))))
         (enumerate-interval 1 4))

;; ex 2.42

;; the representaion for the board position is a pair.

(define make-position cons)

(define get-row car)

(define get-column cdr)

(make-position 2 3)

;; another data structure will be a list of pairs. this will hold the
;; positions of the queens

(define s (list (make-position 2 3) (make-position 3 4)))

(define (adjoin-position new-row k set-of-pos)
  (cons (make-position new-row k) set-of-pos))

(adjoin-position 4 5 s)

(define empty-board '())

;; safe? will take a column and a list of already-safe positions
;; the first position is what we need to test against
(define (safe? k positions)
  (let ((safe-poss (cdr positions))
        (test-row (get-row (car positions)))
        (test-col (get-column (car positions))))
    ;;(display "positions") (display positions) (neline)
    ;;(display "test-pos") (display test-pos) (newline)
    ;;(display "rest") (display rest) (newline)
    (and
     ;; will have 3 tests
     (null?
      ;; right-diag
      (filter (lambda (x) (= (+ test-row test-col) x))
              (map (lambda (xx) (+ (get-row xx) (get-column xx)))
                   safe-poss)))
     (null?
      ;; left diag
      (filter (lambda (x) (= (- test-col test-row) x))
              (map (lambda (xx) (- (get-row xx) (get-column xx)))
                   safe-poss)))
     (null?
      ;; check for same row
      (filter (lambda (x) (= (car x) test-row)) safe-poss))
     )
    )
  )

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0) 
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))


(queens 3)



