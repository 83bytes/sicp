;; Ex 2.1 make a make-rat that can handle any type of negetive integer.

(define (make-rat n d)
  (if (and (< 0 (/ n d)) (< 0 n))
      (let ((g (gcd n d)))
	(cons (/ n g) (/ d g)))
      (make-rat (abs n) (abs d))))


(define (numer x) (car x))

(define (denom x) (cdr x))

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

(define one-half (make-rat 1 2))

(print-rat one-half)

(print-rat (make-rat -2 3))

(print-rat (make-rat 2 -3))

(print-rat (make-rat -4 -16))

;; ex 2.2

(define (make-segment x y) (cons x y))

(define (start-segment x) (car x))

(define (end-segment x) (cdr x))

(define (make-point x y) (cons x y))

(define (x-point x) (car x))

(define (y-point x) (cdr x))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")")
  )

(define (print-segment s)
  (print-point (start-segment s))
  (print-point (end-segment s)))

(print-point (make-point 2 -3))

(define 2-3 (make-point 2 3))

(define 5-6 (make-point 5 6))

(define seg (make-segment 2-3 5-6))


(define (midpoint-segment s)
  (cons (/ (+ (x-point (start-segment s)) (x-point (end-segment s))) 2)
	(/ (+ (y-point (start-segment s)) (y-point (end-segment s))) 2)))

(print-point (midpoint-segment seg))

