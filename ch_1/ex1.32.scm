;; define an accumulator that will do sum and multiplication
(define-module (accumulate-sicp))

(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
		(accumulate combiner null-value term (next a) next b))))

(define (indentity x) x)

(define (next x) (+ x 1))

(accumulate + 0 identity 1 next 5) ;; prints 15

(accumulate * 1 identity 1 next 5) ;; prints 120
