; define a procedur that takes in 3 numbers as arguments and returns
; the sum of the square of the sum of the two larger numbers
(define-module (weridsum-sicp))

(define (weirdSum x y z)
  (cond ((and (< x y) (< x z)) (sum (square y) (square z)))
	((and (< y x) (< y z)) (sum (square x) (square z)))
	((and (< z x) (< z y)) (sum (square x) (square y)))))

(define (sum a b) (+ a b))

(define (square a) (* a a))

(weirdSum 1 2 3)	; prints 13

(weirdSum 3 2 1) ; prints 13

(weirdSum 2 1 3) ; prints 13
