;; this exercise asks us to design a function that will capture the
;; mathematical idea of the the product symbol (pi) and use that to
;; compute the a product expression

(define-module (product-sicp))

(define (next x) (+ x 1))

(define (identity x) x)

(define (prod term a next b)
  (if (> a b)
      1
      (* (term a)
	 (prod term (next a) next b))))


(prod identity 1 next 9)

;; factorial in terms of the product function
(define (factorial x)
  (prod identity 1 next x))

(factorial 5)

;; pi using wallis product
(define (wallis-pi n)
  (define (term x) ;; the idea is to capture the abstraction in each term
    (/ (* 4 x x)
       (- (* 4 x x) 1)))
  (* 2.0 (prod term 1 next n)))

(wallis-pi 100.0)
