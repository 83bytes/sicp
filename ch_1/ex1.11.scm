;; Page: 42
;; a funciton is given and we are required to implement it using
;; an iterative method and
;; a recursive method.

(define-module (rec-sicp))

;; RECURSIVE 
(define (func x)
  (cond ((< x 3) x)
	(else (+ (func (- x 1))
	    (* 2 (func (- x 2)))
	    (* 3 (func( - x 3)))))))

(func 4)



;; ITERATIVE

(define (func2 x)
  (define (func_iter count a b c )
    (cond ((= count 0) a)
	  (else (func_iter
		 (- count 1)
		 b ;; a
		 c ;; b
		 (+ c (* 2 b) (* 3 a)))))) ;; c
  (func_iter x 0 1 2))

(func2 2)

(func2 3)
