;; This exercise wants us to implement a new cube-root funciton using
;; newtons formula


(define (cube-root-iter guess x)
  (if (good-enough? guess x)
      guess
      (cube-root-iter (improve guess x)
		      x)))

(define (improve y x)
  (/ (+ (/ x (square y)) (* 2 y)) 3))

(define (good-enough? y x)
  (< (abs (- (* (square y) y) x)) 0.001 ))

(define (square x) (* x x))

(define (cube-root x)
  (cube-root-iter 1.0 x))

(cube-root 8)   ; Prints 2.0000049116
