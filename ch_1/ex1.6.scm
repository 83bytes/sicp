; This exercise has some prolouge

; First we define a set of procedures to find out the square root of a
; number using newtons method

(define (sqrt-iter guess x)            ; define a procedure
  (if (good-enough? guess x)           ; if the guess is good enough
      guess                            ; the guess is the answer
      (sqrt-iter (improve guess x)     ; otherwise..... call sqrt-iter again with an updated guess
		 x)))                  ; and x :-)

(define (improve guess x)              ; This is the procedure that calculates the new guess.
  (average guess (/ x guess)))         ; it calculates teh new guess by averaging the old guess
				       ; with the ratio of the number and the old guess

(define (average x y)
  (/ (+ x y) 2))

(define (good-enough? guess x)         ; Finds if a guess is good enough
  (< (abs (- (square guess) x))        
     0.001))

(define (square x) (* x x))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(sqrt 3)                               ; PRINTS 1.7321428... ( :-) )

(+ 2 3)



;; Next we define a new-if procedure that implements if in terms of
;; conds and use that to find  the square root

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
	(else else-clause)))

(new-if (= 3 5) 3 5)      ; Prints 5


(define (sqrt-iter-new guess x)
  (new-if (good-enough? guess x)
	  guess
	  (sqrt-iter-new (improve guess x)
			 x)))

(define (sqrt2 x)
  (sqrt-iter-new 1.0 x))

(sqrt2 3) ;; the question is to find out what happens when this procedure is called.

;; That is a trick question.  This is because LISP uses applicative
;; order evaluaiton for procedures. Thus in the procedure new-if all
;; the predicate, the then-clause and the else-clause are going to be
;; evaluated. Thus since in sqrt-iter-new the else-clause of the
;; new-if part is a recursive call to sqrt-iter-new. Thus we have a
;; spectacular infinite loop


