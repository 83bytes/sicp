;; modify fixed point function so that it prints the guess in each iteration
;; use that to find the solutoin of x^x = 1000
(define-module (fixed-sicp))

(define tolerance 0.000001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (display "guess: ")
      (display guess)
      (newline)
      (if (close-enough? guess next)
	  next
	  (try next))))
  (try first-guess))

(fixed-point cos 0.5)

;; 0.7390855263619245

;; without average damping
(fixed-point (lambda (x) (/
			  (log 1000)
			  (log x)))
	     2.0)

;; 4.555535 (39 steps)


;; with average damping
(define (average x y) (/ (+ x y) 2))


(fixed-point (lambda (x)
	       (average x
			(/ (log 1000) (log x))))
	     2.0)

;; 4.555535 (10 steps)

