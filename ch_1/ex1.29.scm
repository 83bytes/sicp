;; the exercise asks us to write a procedure to calculate the integral
;; using simpsons rule. (this took me about 1 week to design on my own
;; with some help from the scheme wiki)

(define-module (simpsons-sicp))

(define (cube x) (* x x x))
(define (next x) (+ x 1))


;; this is a linear recursive function
(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
	 (sum term (next a) next b))))

;; calculates the integral with the simpsons rule
(define (simprule a b n f)
  (define h (/ (- b a) n))
  (define (yk k) (f (+ a (* k h))))
  (define (add2 a) (+ 2 a))
  (/ (* h (+ (yk 0)
	     (yk 1)
	     (* 4 (sum yk 1 add2 (- n 1)))
	     (* 2 (sum yk 2 add2 (- n 2)))))
     3))


(simprule 0 1 1000 cube)


(next 2)


